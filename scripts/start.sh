#!/bin/bash
#
# RetroAL Setup Script 0.3
#
# Daniel Menelkir <menelkir@itroll.org>

# You shouldn't execute this script as root

if [[ $EUID == 0 ]]; then
	echo "This script shouldn't be run as root"
	exit 1
fi

# If you don't like yaourt, just change to whatever you want

export AURT='/usr/bin/yay --noconfirm'
export DIALOG_CANCEL=1
export DIALOG_ESC=255
export HEIGHT=0
export WIDTH=0

# The roms are supposed to be inside the folder $HOME/Emulation by default
# If you don't like this path, you'll need to modify emulationstation configurations too

if [[ ! -e $HOME/Emulation ]]; then
	mkdir -p $HOME/Emulation
fi

# Initial Menus

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Install Packages" \
			"2" "Setup" \
			"3" "Upgrade" \
			"4" "Reboot" \
			"5" "Shutdown" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
	   	exec ./scripts/setup/packages.sh
      	;;
    	2 )
	   	exec ./scripts/setup/setup.sh
      	;;
    	3 ) 
		exec ./scripts/setup/update.sh
      	;;
	4 )
		exec ./scripts/system/reboot.sh
	;;
	5 )
		exec ./scripts/system/shutdown.sh
	;;
  	esac
done

