#!/bin/bash
#
# Core Downloader for RetroAL 0.5
#
# Daniel Menelkir <menelkir@itroll.org>

ARCH=`uname -m`
COREPATH=/home/$USER/.config/retroarch/cores
DIALOG_CANCEL=1
DIALOG_ESC=255
HEIGHT=0
WIDTH=0

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL - Cores Download" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 20 \
                        "00" "CLEAN                            Clean core zipfiles from core directory" \
			"01" "2048		 		2048 Puzzle Game" \
			"02" "4do				Panasonic 3DO" \
			"03" "81				Sinclair ZX-81" \
			"04" "atari800				Atari 800" \
			"05" "bluemsx 				MSX" \
			"06" "bsnes				Super Nintendo" \
			"07" "bsnes-mercury  			Super Nintendo (with performance improvements)" \
			"08" "cap32 				Amstrad CPC" \
			"09" "higan-balanced 			Super Nintendo" \
			"10" "desmume 				Nintendo DS" \
			"11" "dosbox 				MS-DOS" \
			"12" "fbalpha				Final Burn Alpha" \
			"13" "fceumm				Nintendo Entertainment System" \
			"14" "fmsx				MSX" \
			"15" "fuse			        Sinclair ZX-Spectrum" \
			"16" "gambatte		                Nintendo Game Boy / Game Boy Color" \
			"17" "genesis-plus-gx	                Sega GameGear / Mega Drive / Master System / SegaCD" \
			"18" "freeintv		                Mattel Intellivision" \
			"19" "gw 			        Game & Watch" \
			"20" "handy		                Atari Lynx" \
			"21" "hatari                           Atari SE/STE/TT/Falcon" \
			"22" "lutro 		                Lua game framework" \
			"23" "mame                             Multiple Arcade Machine Emulator (trunk)" \
			"24" "mame2000		                Multiple Arcade Machine Emulator 0.37b5" \
			"25" "mame2003		                Multiple Arcade Machine Emulator 0.78" \
			"26" "mame2014		                Multiple Arcade Machine Emulator 0.159" \
			"27" "mame2016		                Multiple Arcade Machine Emulator 0.174" \
			"28" "mednafen-ngp                     NeoGeo Pocket / Pocket color" \
			"29" "mednafen-pce-fast                Nec PC Engine" \
			"30" "mednafen-psx                     Sony Playstation" \
			"31" "mednafen-psx-hw	                Sony Playstation (Hardware accelerated)" \
			"32" "mednafen-saturn	                Sega Saturn" \
			"33" "mednafen-supergrafx              NEC Supergrafx" \
			"34" "mednafen-wswan                   Bandai Wonderswan / Wonderswan Color" \
			"35" "mgba                             Nintendo Game Boy Advance" \
			"36" "mupen64plus	                Nintendo 64 (with dynarec support)" \
			"37" "nestopia	                        Nintendo Entertainment System" \
			"38" "nxengine		                NX Engine (Cave Story)" \
			"39" "o2em                             Magnavox Odyssey 2 / Philips Videopac" \
			"40" "parallel-n64		        Nintendo 64 (with dynarec and vulkan support)" \
			"41" "picodrive                        Sega Master System / Megadrive / 32X / SegaCD" \
			"42" "ppsspp		                Sony PSP" \
			"43" "prboom		                Doom" \
			"44" "prosystem                        Atari 7800" \
			"45" "quicknes		                Nintendo Entertainment System" \
			"46" "reicast		                Sega Dreamcast" \
			"47" "scummvm		                ScummVM" \
			"48" "snes9x		                Super Nintendo (Snes9x trunk)" \
			"49" "snes9x2010		        Super Nintendo (Snes9x-2010)" \
			"50" "stella 		                Atari 2600" \
			"51" "tgbdual		                Nintendo Game Boy Color" \
			"52" "vba-next		                Game Boy Advance" \
			"53" "vecx                             GCE Vectrex" \
			"54" "virtualjaguar	                Atari Jaguar" \
			"55" "yabause		                Sega Saturn" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./scripts/setup/packages.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./scripts/setup/packages.sh
      			exit 1
      		;;
  	esac
  	case $selection in
        00 )
                clear
                rm /$COREPATH/*.zip
        ;;
        01 )
                clear  
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/2048_libretro.so.zip -o $COREPATH 
                exec unzip /$COREPATH/2048_libretro.so.zip 
                        if [[ ! -e $HOME/Emulation/2048 ]]; then
                                mkdir -p $HOME/Emulation/2048
                        fi
        ;;
        02 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/3do_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/3do_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/3do ]]; then
                                mkdir -p $HOME/Emulation/3do
                        fi
        ;;
        03 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/81_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/81_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/81 ]]; then
                                mkdir -p $HOME/Emulation/81
                        fi
        ;;
        04 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/atari800_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/atari800_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/atari800 ]]; then
                                mkdir -p $HOME/Emulation/atari800
                        fi
        ;;    	
        05 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/bluemsx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/bluemsx_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/msx ]]; then
                                mkdir -p $HOME/Emulation/msx
                        fi
        ;;
        06 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/bsnes_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/bsnes_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/snes ]]; then
                                mkdir -p $HOME/Emulation/snes
                        fi
      	;;    	
	07 )
         	clear
          	exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/bsnes_mercury_balanced_libretro.so.zip -o $COREPATH
            	exec unzip /$COREPATH/bsnes_mercury_balanced_libretro.so.zip
			if [[ ! -e $HOME/Emulation/snes ]]; then
                 		mkdir -p $HOME/Emulation/snes
                	fi
      	;;
    	08 )
                clear
            	exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/cap32_libretro.so.zip -o $COREPATH
            	exec unzip /$COREPATH/cap32_libretro.so.zip
			if [[ ! -e $HOME/Emulation/amstradcpc ]]; then
               			mkdir -p $HOME/Emulation/amstradcpc
               		fi
	;;    	
        09 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/higan_sfc_balanced_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/higan_sfc_balanced_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/snes ]]; then
                                mkdir -p $HOME/Emulation/snes
                        fi
        ;;
        10 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/desmume_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/desmume_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/nds ]]; then
                                mkdir -p $HOME/Emulation/nds
                        fi
        ;;
        11 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/dosbox_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/dosbox_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/dos ]]; then
                                mkdir -p $HOME/Emulation/dos
                        fi
        ;;
        12 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/fbalpha_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/fbalpha_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/fba ]]; then
                                mkdir -p $HOME/Emulation/fba
                        fi
        ;;
        13 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/fceumm_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/fceumm_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/nes ]]; then
                                mkdir -p $HOME/Emulation/nes
                        fi
        ;;
        14 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/fmsx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/fmsx_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/c64 ]]; then
                                mkdir -p $HOME/Emulation/c64
                        fi
        ;;    	
        15 )
                clear 
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/fuse_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/fuse_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/zxspectrum ]]; then
                                mkdir -p $HOME/Emulation/zxspectrum
                        fi
        ;;
        16 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/gambatte_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/gambatte_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/gb ]]; then
                                mkdir -p $HOME/Emulation/gb
                        fi
                        if [[ ! -e $HOME/Emulation/gbc ]]; then
                                mkdir -p $HOME/Emulation/gbc
                        fi
        ;;    	
        17 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/genesis_plus_gx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/genesis_plus_gx_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/megadrive ]]; then
                                mkdir -p $HOME/Emulation/megadrive
                        fi
        ;;
        18 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/freeintv_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/freeintv_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/intellivision ]]; then
                                mkdir -p $HOME/Emulation/intellivision
                        fi
        ;;    	
        19 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/gw_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/gw_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/gw ]]; then
                                mkdir -p $HOME/Emulation/gw
                        fi
      	;;
    	20 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/handy_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/handy_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/atarilynx ]]; then
                                mkdir -p $HOME/Emulation/lynx
                        fi
      	;;
    	21 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/hatari_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/hatari_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/atarilynx ]]; then
                                mkdir -p $HOME/Emulation/atarist
                        fi
      	;;
    	22 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/lutro_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/lutro_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/lutro ]]; then
                                mkdir -p $HOME/Emulation/lutro
                        fi
      	;;
    	23 )
	   	clear 
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mame_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mame_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/mame ]]; then
                                mkdir -p $HOME/Emulation/mame
                        fi
      	;;
    	24 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mame2000_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mame2000_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/mame ]]; then
                                mkdir -p $HOME/Emulation/mame
                        fi
      	;;    	
	25 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mame2003_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mame2003_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/mame ]]; then
                                mkdir -p $HOME/Emulation/mame
                        fi
      	;;
    	26 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mame2014_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mame2014_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/mame ]]; then
                                mkdir -p $HOME/Emulation/mame
                        fi
      	;;    	
	27 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mame2016_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mame2000_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/mame ]]; then
                                mkdir -p $HOME/Emulation/mame
                        fi
      	;;
    	28 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_ngp_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_ngp_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/ngp ]]; then
                                mkdir -p $HOME/Emulation/ngp
                        fi
      	;;    	
	29 )
	   	clear 
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_pce_fast_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_pce_fast_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/pce ]]; then
                                mkdir -p $HOME/Emulation/pce
                        fi
      	;;
    	30 )
	   	clear 
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_psx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_psx_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/psx ]]; then
                                mkdir -p $HOME/Emulation/psx
                        fi
      	;;
   	31 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_psx_hw_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_psx_hw_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/psx ]]; then
                                mkdir -p $HOME/Emulation/psx
                        fi
      	;;
    	32 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_saturn_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_saturn_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/saturn ]]; then
                                mkdir -p $HOME/Emulation/saturn
                        fi
      	;;
    	33 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_supergrafx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_supergrafx_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/supergrafx ]]; then
                                mkdir -p $HOME/Emulation/supergrafx
                        fi
      	;;
    	34 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mednafen_wswan_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mednafen_wswan_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/wswan ]]; then
                                mkdir -p $HOME/Emulation/wswan
                        fi
      	;;    	
	35 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mgba_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mgba_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/gba ]]; then
                                mkdir -p $HOME/Emulation/gba
                        fi
      	;;
    	36 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/mupen64plus_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/mupen64plus_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/n64 ]]; then
                                mkdir -p $HOME/Emulation/n64
                        fi
      	;;    	
	37 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/nestopia_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/nestopia_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/nes ]]; then
                                mkdir -p $HOME/Emulation/nes
                        fi
      	;;
    	38 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/nxengine_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/nxengine_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/nxengine ]]; then
                                mkdir -p $HOME/Emulation/nxengine
                        fi
      	;;    	
	39 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/o2em_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/o2em_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/o2em ]]; then
                                mkdir -p $HOME/Emulation/o2em
                        fi
      	;;
    	40 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/parallel_n64_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/parallel_n64_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/n64 ]]; then
                                mkdir -p $HOME/Emulation/n64
                        fi
      	;;
   	41 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/picodrive_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/picodrive_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/megadrive ]]; then
                                mkdir -p $HOME/Emulation/megadrive
                        fi
      	;;
    	42 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/ppsspp_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/ppsspp_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/psp ]]; then
                                mkdir -p $HOME/Emulation/psp
                        fi
      	;;
    	43 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/prboom_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/prboom_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/prboom ]]; then
                                mkdir -p $HOME/Emulation/prboom
                        fi
      	;;
    	44 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/prosystem_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/prosystem_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/atari7800 ]]; then
                                mkdir -p $HOME/Emulation/atari7800
                        fi
      	;;    	
	45 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/quicknes_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/quicknes_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/nes ]]; then
                                mkdir -p $HOME/Emulation/nes
                        fi
      	;;
    	46 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/reicast_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/reicast_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/dreamcast ]]; then
                                mkdir -p $HOME/Emulation/dreamcast
                        fi
      	;;    	
	47 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/scummvm_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/scummvm_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/scummvm ]]; then
                                mkdir -p $HOME/Emulation/scummvm
                        fi
      	;;
    	48 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/snes9x_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/snes9x_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/snes ]]; then
                                mkdir -p $HOME/Emulation/snes
                        fi
      	;;    	
	49 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/snes9x2010_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/snes9x2010_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/snes ]]; then
                                mkdir -p $HOME/Emulation/snes
                        fi
      	;;
    	50 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/stella_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/stella_libretro.so.zip
                        if [[ ! -e $HOME/Emulation/atari2600 ]]; then
                                mkdir -p $HOME/Emulation/atari2600
                        fi
      	;;
  	51 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/tgbdual_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/tgbdual_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/gbc ]]; then
                                mkdir -p $HOME/Emulation/gbc
                        fi
      	;;
    	52 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/vba_next_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/vba_next_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/gba ]]; then
                                mkdir -p $HOME/Emulation/gba
                        fi
      	;;
    	53 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/vecx_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/vecx_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/vectrex ]]; then
                                mkdir -p $HOME/Emulation/vectrex
                        fi
      	;;
    	54 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/virtualjaguar_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/virtualjaguar_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/atarijaguar ]]; then
                                mkdir -p $HOME/Emulation/atarijaguar
                        fi
      	;;    	
	55 )
                clear
                exec wget -c http://buildbot.libretro.com/nightly/linux/$ARCH/latest/yabause_libretro.so.zip -o $COREPATH
                exec unzip /$COREPATH/yabause_libretro.so.zip
		        if [[ ! -e $HOME/Emulation/saturn ]]; then
                                mkdir -p $HOME/Emulation/saturn
                        fi
      	;;
  	esac
done

