#!/bin/bash
#
# RetroAL Themes Downloader - 0.2
#
# Daniel Menelkir <menelkir@itroll.org>

# Creating Emulationstation themes directory


# Initial Menus

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL - EmulationStation Themes Downloader" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Carbon" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./retroal_setup.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./retroal_setup.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
		if [[ ! -e $HOME/.emulationstation/themes ]]; then
                        mkdir -p $HOME/.emulationstation/themes
                fi
		if [[ ! -e $HOME/.emulationstation/themes/es-theme-carbon.git ]]; then
			git clone https://github.com/RetroPie/es-theme-carbon.git $HOME/.emulationstation/themes
		fi
      	;;
  	esac
done

