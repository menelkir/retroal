#!/bin/bash
#
# Core Downloader for RetroAL 0.6
#
# Daniel Menelkir <menelkir@itroll.org>

DIALOG_CANCEL=1
DIALOG_ESC=255
HEIGHT=0
WIDTH=0

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL - Cores Download" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 20 \
			"01" "libretro-2048-git 		2048 Puzzle Game" \
			"02" "libretro-4do-git			Panasonic 3DO" \
			"03" "libretro-81-git			Sinclair ZX-81" \
			"04" "libretro-atari800-git		Atari 800" \
			"05" "libretro-bluemsx-git 		MSX" \
			"06" "libretro-bsnes-git		Super Nintendo" \
			"07" "libretro-bsnes-mercury-git	Super Nintendo (with performance improvements)" \
			"08" "libretro-cap32-git 		Amstrad CPC" \
			"09" "libretro-catsfc-git 		Super Nintendo" \
			"10" "libretro-desmume-git 		Nintendo DS" \
			"11" "libretro-dosbox-git 		MS-DOS" \
			"12" "libretro-fbalpha-git		Final Burn Alpha" \
			"13" "libretro-fceumm-git		Nintendo Entertainment System" \
			"14" "libretro-frodo-git		Commodore 64" \
			"15" "libretro-fuse-git			Sinclair ZX-Spectrum" \
			"16" "libretro-gambatte-git		Nintendo Game Boy / Game Boy Color" \
			"17" "libretro-genesis-plus-gx-git	Sega GameGear / Mega Drive / Master System / SegaCD" \
			"18" "libretro-glupen64-git		Nintendo 64" \
			"19" "libretro-gw-git 			Game & Watch" \
			"20" "libretro-handy-git		Atari Lynx" \
			"21" "libretro-hatari-enhanced-git	Atari SE/STE/TT/Falcon (with IPF Support)" \
			"22" "libretro-hatari-git 		Atari SE/STE/TT/Falcon" \
			"23" "libretro-mame-git			Multiple Arcade Machine Emulator (trunk)" \
			"24" "libretro-mame2000-git		Multiple Arcade Machine Emulator 0.37b5" \
			"25" "libretro-mame2003-git		Multiple Arcade Machine Emulator 0.78" \
			"26" "libretro-mame2014-git		Multiple Arcade Machine Emulator 0.159" \
			"27" "libretro-mame2016-git		Multiple Arcade Machine Emulator  0.174" \
			"28" "libretro-mednafen-ngp-git		NeoGeo Pocket / Pocket color" \
			"29" "libretro-mednafen-pce-fast-git 	Nec PC Engine" \
			"30" "libretro-mednafen-psx-git		Sony Playstation" \
			"31" "libretro-mednafen-psx-hw-git	Sony Playstation (Hardware accelerated)" \
			"32" "libretro-mednafen-saturn-git	Sega Saturn" \
			"33" "libretro-mednafen-supergrafx-git	NEC Supergrafx" \
			"34" "libretro-mednafen-wswan-git	Bandai Wonderswan / Wonderswan Color" \
			"35" "libretro-mgba-git			Nintendo Game Boy Advance" \
			"36" "libretro-mupen64plus-git		Nintendo 64 (with dynarec support)" \
			"37" "libretro-nestopia-git		Nintendo Entertainment System" \
			"38" "libretro-nxengine-git		NX Engine (Cave Story)" \
			"39" "libretro-o2em-git			Magnavox Odyssey 2 / Philips Videopac" \
			"40" "libretro-parallel-git		Nintendo 64 (with dynarec and vulkan support)" \
			"41" "libretro-picodrive-git		Sega Master System / Megadrive / 3X / SegaCD" \
			"42" "libretro-ppsspp-git		Sony PSP" \
			"43" "libretro-prboom-git		Doom" \
			"44" "libretro-prosystem-git		Atari 7800" \
			"45" "libretro-quicknes-git		Nintendo Entertainment System" \
			"46" "libretro-reicast-git		Sega Dreamcast" \
			"47" "libretro-scummvm-git		ScummVM" \
			"48" "libretro-snes9x-git		Super Nintendo (Snes9x trunk)" \
			"49" "libretro-snes9x2010-git		Super Nintendo (Snes9x-2010)" \
			"50" "libretro-stella-git 		Atari 2600" \
			"51" "libretro-tgbdual-git		Nintendo Game Boy Color" \
			"52" "libretro-vba-next-git		Game Boy Advance" \
			"53" "libretro-vecx-git			GCE Vectrex" \
			"54" "libretro-virtualjaguar-git	Atari Jaguar" \
			"55" "libretro-yabause-git		Sega Saturn" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./scripts/setup/packages.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./scripts/setup/packages.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	01 )
		clear 
		exec $AURT -S libretro-2048-git
		if [[ ! -e $HOME/Emulation/2048 ]]; then
			mkdir -p $HOME/Emulation/2048
		fi
      	;;
    	02 )
	   	clear 
		exec $AURT -S libretro-4do-git
                if [[ ! -e $HOME/Emulation/3do ]]; then
                        mkdir -p $HOME/Emulation/3do
                fi
      	;;
    	03 )
	   	clear 
		exec $AURT -S libretro-81-git
                if [[ ! -e $HOME/Emulation/81 ]]; then
                        mkdir -p $HOME/Emulation/81
                fi
      	;;
    	04 )
	   	clear 
		exec $AURT -S libretro-atari800-git
                if [[ ! -e $HOME/Emulation/atari800 ]]; then
                        mkdir -p $HOME/Emulation/atari800
                fi
      	;;    	
	05 )
	   	clear 
		exec $AURT -S libretro-bluemsx-git
                if [[ ! -e $HOME/Emulation/msx ]]; then
                        mkdir -p $HOME/Emulation/msx
                fi
      	;;
    	06 )
	   	clear 
		exec $AURT -S libretro-bsnes-git
		if [[ ! -e $HOME/Emulation/snes ]]; then
                        mkdir -p $HOME/Emulation/snes
                fi
      	;;    	
	07 )
	   	clear 
		exec $AURT -S libretro-bsnes-mercury-git
		if [[ ! -e $HOME/Emulation/snes ]]; then
                        mkdir -p $HOME/Emulation/snes
                fi
      	;;
    	08 )
	   	clear 
		exec $AURT -S libretro-cap32-git
		if [[ ! -e $HOME/Emulation/amstradcpc ]]; then
                        mkdir -p $HOME/Emulation/amstradcpc
                fi
      	;;    	
	09 )
	   	clear 
		exec $AURT -S libretro-catsfc-git
		if [[ ! -e $HOME/Emulation/snes ]]; then
                        mkdir -p $HOME/Emulation/snes
                fi
      	;;
    	10 )
	   	clear 
		exec $AURT -S libretro-desmume-git
		if [[ ! -e $HOME/Emulation/nds ]]; then
                        mkdir -p $HOME/Emulation/nds
                fi
      	;;
   	11 )
	   	clear 
		exec $AURT -S libretro-dosbox-git
		if [[ ! -e $HOME/Emulation/dos ]]; then
                        mkdir -p $HOME/Emulation/dos
                fi
      	;;
    	12 )
	   	clear 
		exec $AURT -S libretro-fbalpha-git
		if [[ ! -e $HOME/Emulation/fba ]]; then
                        mkdir -p $HOME/Emulation/fba
                fi
      	;;
    	13 )
	   	clear 
		exec $AURT -S libretro-fceumm-git
		if [[ ! -e $HOME/Emulation/nes ]]; then
                        mkdir -p $HOME/Emulation/nes
                fi
      	;;
    	14 )
	   	clear 
		exec $AURT -S libretro-frodo-git
		if [[ ! -e $HOME/Emulation/c64 ]]; then
                        mkdir -p $HOME/Emulation/c64
                fi
      	;;    	
	15 )
	   	clear 
		exec $AURT -S libretro-fuse-git
		if [[ ! -e $HOME/Emulation/zxspectrum ]]; then
                        mkdir -p $HOME/Emulation/zxspectrum
                fi
      	;;
    	16 )
	   	clear 
		exec $AURT -S libretro-gambatte-git
		if [[ ! -e $HOME/Emulation/gb ]]; then
                        mkdir -p $HOME/Emulation/gb
                fi
		if [[ ! -e $HOME/Emulation/gbc ]]; then
                        mkdir -p $HOME/Emulation/gbc
                fi
      	;;    	
	17 )
	   	clear 
		exec $AURT -S libretro-genesis-plus-gx-git
		if [[ ! -e $HOME/Emulation/megadrive ]]; then
                        mkdir -p $HOME/Emulation/megadrive
                fi
      	;;
    	18 )
	   	clear 
		exec $AURT -S libretro-glupen64-git
		if [[ ! -e $HOME/Emulation/n64 ]]; then
                        mkdir -p $HOME/Emulation/n64
                fi
      	;;    	
	19 )
	   	clear 
		exec $AURT -S libretro-gw-git 
		if [[ ! -e $HOME/Emulation/gw ]]; then
                        mkdir -p $HOME/Emulation/gw
                fi
      	;;
    	20 )
	   	clear 
		exec $AURT -S libretro-handy-git
		if [[ ! -e $HOME/Emulation/atarilynx ]]; then
                        mkdir -p $HOME/Emulation/lynx
                fi
      	;;
    	21 )
		clear 
		exec $AURT -S libretro-hatari-enhanced-git
		if [[ ! -e $HOME/Emulation/atarilynx ]]; then
                        mkdir -p $HOME/Emulation/atarist
                fi
      	;;
    	22 )
	   	clear 
		exec $AURT -S libretro-hatari-git 
		if [[ ! -e $HOME/Emulation/atarilynx ]]; then
                        mkdir -p $HOME/Emulation/atarist
                fi
      	;;
    	23 )
	   	clear 
		exec $AURT -S libretro-mame-git
		if [[ ! -e $HOME/Emulation/mame ]]; then
                        mkdir -p $HOME/Emulation/mame
                fi
      	;;
    	24 )
	   	clear 
		exec $AURT -S libretro-mame2000-git
		if [[ ! -e $HOME/Emulation/mame ]]; then
                        mkdir -p $HOME/Emulation/mame
                fi
      	;;    	
	25 )
	   	clear 
		exec $AURT -S libretro-mame2003-git
		if [[ ! -e $HOME/Emulation/mame ]]; then
                        mkdir -p $HOME/Emulation/mame
                fi
      	;;
    	26 )
	   	clear 
		exec $AURT -S libretro-mame2014-git
		if [[ ! -e $HOME/Emulation/mame ]]; then
                        mkdir -p $HOME/Emulation/mame
                fi
      	;;    	
	27 )
	   	clear 
		exec $AURT -S libretro-mame2016-git
		if [[ ! -e $HOME/Emulation/mame ]]; then
                        mkdir -p $HOME/Emulation/mame
                fi
      	;;
    	28 )
	   	clear 
		exec $AURT -S libretro-mednafen-ngp-git
		if [[ ! -e $HOME/Emulation/ngp ]]; then
                        mkdir -p $HOME/Emulation/ngp
                fi
      	;;    	
	29 )
	   	clear 
		exec $AURT -S libretro-mednafen-pce-fast-git
		if [[ ! -e $HOME/Emulation/pce ]]; then
                        mkdir -p $HOME/Emulation/pce
                fi
      	;;
    	30 )
	   	clear 
		exec $AURT -S libretro-mednafen-psx-git
		if [[ ! -e $HOME/Emulation/psx ]]; then
                        mkdir -p $HOME/Emulation/psx
                fi
      	;;
   	31 )
	   	clear 
		exec $AURT -S libretro-mednafen-psx-hw-git
		if [[ ! -e $HOME/Emulation/psx ]]; then
                        mkdir -p $HOME/Emulation/psx
                fi
      	;;
    	32 )
	   	clear 
		exec $AURT -S libretro-mednafen-saturn-git
		if [[ ! -e $HOME/Emulation/saturn ]]; then
                        mkdir -p $HOME/Emulation/saturn
                fi
      	;;
    	33 )
	   	clear 
		exec $AURT -S libretro-mednafen-supergrafx-git
		if [[ ! -e $HOME/Emulation/supergrafx ]]; then
                        mkdir -p $HOME/Emulation/supergrafx
                fi
      	;;
    	34 )
	   	clear 
		exec $AURT -S libretro-mednafen-wswan-git
		if [[ ! -e $HOME/Emulation/wswan ]]; then
                        mkdir -p $HOME/Emulation/wswan
                fi
      	;;    	
	35 )
	   	clear 
		exec $AURT -S libretro-mgba-git
		if [[ ! -e $HOME/Emulation/gba ]]; then
                        mkdir -p $HOME/Emulation/gba
                fi
      	;;
    	36 )
	   	clear 
		exec $AURT -S libretro-mupen64plus-git
		if [[ ! -e $HOME/Emulation/n64 ]]; then
                        mkdir -p $HOME/Emulation/n64
                fi
      	;;    	
	37 )
	   	clear 
		exec $AURT -S libretro-nestopia-git
		if [[ ! -e $HOME/Emulation/nes ]]; then
                        mkdir -p $HOME/Emulation/nes
                fi
      	;;
    	38 )
	   	clear 
		exec $AURT -S libretro-nxengine-git
		if [[ ! -e $HOME/Emulation/cavestory ]]; then
                        mkdir -p $HOME/Emulation/cavestory
                fi
      	;;    	
	39 )
	   	clear
		exec $AURT -S libretro-o2em-git	
		if [[ ! -e $HOME/Emulation/videopac ]]; then
                        mkdir -p $HOME/Emulation/videopac
                fi
      	;;
    	40 )
	   	clear 
		exec $AURT -S libretro-parallel-git
		if [[ ! -e $HOME/Emulation/n64 ]]; then
                        mkdir -p $HOME/Emulation/n64
                fi
      	;;
   	41 )
	   	clear 
		exec $AURT -S libretro-picodrive-git
		if [[ ! -e $HOME/Emulation/megadrive ]]; then
                        mkdir -p $HOME/Emulation/megadrive
                fi
      	;;
    	42 )
	   	clear 
		exec $AURT -S libretro-ppsspp-git
		if [[ ! -e $HOME/Emulation/psp ]]; then
                        mkdir -p $HOME/Emulation/psp
                fi
      	;;
    	43 )
	   	clear 
		exec $AURT -S libretro-prboom-git
		if [[ ! -e $HOME/Emulation/doom ]]; then
                        mkdir -p $HOME/Emulation/doom
                fi
      	;;
    	44 )
	   	clear 
		exec $AURT -S libretro-prosystem-git
		if [[ ! -e $HOME/Emulation/atari7800 ]]; then
                        mkdir -p $HOME/Emulation/atari7800
                fi
      	;;    	
	45 )
	   	clear 
		exec $AURT -S libretro-quicknes-git
		if [[ ! -e $HOME/Emulation/nes ]]; then
                        mkdir -p $HOME/Emulation/nes
                fi
      	;;
    	46 )
	   	clear 
		exec $AURT -S libretro-reicast-git
		if [[ ! -e $HOME/Emulation/dreamcast ]]; then
                        mkdir -p $HOME/Emulation/dreamcast
                fi
      	;;    	
	47 )
	   	clear 
		exec $AURT -S libretro-scummvm-git
		if [[ ! -e $HOME/Emulation/scummvm ]]; then
                        mkdir -p $HOME/Emulation/scummvm
                fi
      	;;
    	48 )
	   	clear 
		exec $AURT -S libretro-snes9x-git
		if [[ ! -e $HOME/Emulation/snes ]]; then
                        mkdir -p $HOME/Emulation/snes
                fi
      	;;    	
	49 )
	   	clear 
		exec $AURT -S libretro-snes9x2010-git	
		if [[ ! -e $HOME/Emulation/snes ]]; then
                        mkdir -p $HOME/Emulation/snes
                fi
      	;;
    	50 )
	   	clear 
		exec $AURT -S libretro-stella-git
		if [[ ! -e $HOME/Emulation/atari2600 ]]; then
                        mkdir -p $HOME/Emulation/atari2600
                fi
      	;;
  	51 )
	   	clear 
		exec $AURT -S libretro-tgbdual-git
		if [[ ! -e $HOME/Emulation/pce ]]; then
                        mkdir -p $HOME/Emulation/pce
                fi
		if [[ ! -e $HOME/Emulation/pcecd ]]; then
                        mkdir -p $HOME/Emulation/pcecd
                fi
      	;;
    	52 )
	   	clear 
		exec $AURT -S libretro-vba-next-git
		if [[ ! -e $HOME/Emulation/gba ]]; then
                        mkdir -p $HOME/Emulation/gba
                fi
      	;;
    	53 )
	   	clear 
		exec $AURT -S libretro-vecx-git
		if [[ ! -e $HOME/Emulation/vectrex ]]; then
                        mkdir -p $HOME/Emulation/vectrex
                fi
      	;;
    	54 )
	   	clear 
		exec $AURT -S libretro-virtualjaguar-git
		if [[ ! -e $HOME/Emulation/atarijaguar ]]; then
                        mkdir -p $HOME/Emulation/atarijaguar
                fi
      	;;    	
	55 )
	   	clear 
		exec $AURT -S libretro-yabause-git
		if [[ ! -e $HOME/Emulation/saturn ]]; then
                        mkdir -p $HOME/Emulation/saturn
                fi
      	;;
  	esac
done

