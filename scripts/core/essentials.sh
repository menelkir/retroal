#!/bin/bash
#
# Essentials Downloader 0.3
#
# Daniel Menelkir <menelkir@itroll.org>

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL - Essentials Downloader" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Emulation Station" \
			"2" "Retroarch" \
			"3" "Download ES default configuration" \
			"4" "Download ES themes" \
			"5" "Download some free games"
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./scripts/setup/packages.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./scripts/setup/packages.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
                if [[ ! -e $HOME/.emulationstation ]]; then
                        mkdir -p $HOME/.emulationstation
                fi
	   	exec $AURT -S emulationstation emulationstation-autoscraper emulationstation-themes
      	;;
    	2 )
	   	exec $AURT -S retroarch retroarch-assets-xmb retroarch-autoconfig-udev 
      	;;
	3) 	
		curl -o $HOME/.emulationstation/es_systems.cfg https://gitlab.com/menelkir/configurations/raw/master/misc/emulationstation/es_systems.cfg
		curl -o $HOME/.emulationstation/es_settings.cfg https://gitlab.com/menelkir/configurations/raw/master/misc/emulationstation/es_settings.cfg
	;;
	4) 
		exec ./scripts/core/themes.sh
	;;
	5)
		exec ./scripts/setup/freegames.sh
	;;	
	* )	exec ./retroal_setup.sh
  	esac
done

