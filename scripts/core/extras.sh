#!/bin/bash
#
# RetroAL Extras Downloader 0.1
#
# Daniel Menelkir <menelkir@itroll.org>

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL - Extras Downloader" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Kodi" \
			"2" "Samba" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./scripts/setup/packages.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./scripts/setup/packages.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
	   	exec $AURT -S kodi kodi-addons
      	;;
    	2 )
	   	exec $AURT -S samba 
      	;;
  	esac
done

