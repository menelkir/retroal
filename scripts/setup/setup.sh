#!/bin/bash
#
# RetroAL Internal Setup 0.1
#
# Daniel Menelkir <menelkir@itroll.org>

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL Internal Setup" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Set Autologin for the user $USER" \
			"2" "Run Emulationstation at boot for user $USER" \
			"3" "Scraper" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
      			clear
			exec ./retroal_setup.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			clear
			exec ./retroal_setup.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
		# I know it's ugly. I accept better solutions for this one
		cp  /lib/systemd/system/getty@.service /tmp/retroal.txt
		cat /tmp/retroal.txt | sed -e s/noclear/"noclear -a $USER"/g > /tmp/retroal2.txt
		sudo cp /tmp/retroal2.txt /lib/systemd/system/getty@.service
		# Making sure everything is working as expected, I had a problem with some scenarios
		sudo systemctl disable getty@.service
		sudo systemctl enable getty@.service
		rm /tmp/retroal.txt
		rm /tmp/retroal2.txt
		sleep 2
      	;;
    	2 )
		echo emulationstation >> /home/$USER/.bash_profile
		sleep 2
      	;;
    	3 )
		echo "Not working yet"
		sleep 2
      	;;
  	esac
done

