#!/bin/bash
#
# RetroAL Updater 0.2
#
# Daniel Menelkir <menelkir@itroll.org>


while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL Updater" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Zero Tolerance for Sega Megadrive" \
			"2" "Mighty Mighty Missle for Sega Megadrive" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
			echo $PWD
      			exec ./retroal_setup.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			exec ./retroal_setup.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
	1 )	
		if [[ ! -e $HOME/Emulation/megadrive ]]; then
                        mkdir -p $HOME/Emulation/megadrive
                fi
		curl -o $HOME/Emulation/megadrive http://retroachievements.org/bin/ZEROTOL.zip 

	;;
    	2 )
                if [[ ! -e $HOME/Emulation/megadrive ]]; then
                        mkdir -p $HOME/Emulation/megadrive
                fi
                curl -o $HOME/Emulation/megadrive http://retroachievements.org/bin/MIGHTY2K.zip
	;;
	* )
		exec ./retroal_setup.sh
	;;
  	esac
done

