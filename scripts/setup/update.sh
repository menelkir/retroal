#!/bin/bash
#
# RetroAL Updater 0.2
#
# Daniel Menelkir <menelkir@itroll.org>


while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL Updater" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Update retroAL to the last revision" \
			"1" "Update all system packages" \
			"2" "Update all packages (including AUR)" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
			echo $PWD
      			exec ./retroal_setup.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			exec ./retroal_setup.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
	1 )	
		git push
	;;
    	2 )
		sudo pacman -Syu
      	;;
    	3 )
		$AURT -Syu
      	;;
	* )
		exec ./retroal_setup.sh
	;;
  	esac
done

