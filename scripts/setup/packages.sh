#!/bin/bash
#
# RetroAL Packages Installer 0.2
#
# Daniel Menelkir <menelkir@itroll.org>

while true; do
	exec 3>&1
	selection=$(dialog \
		--backtitle "RetroAL Packages Installer" \
		--clear \
		--cancel-label "Exit" \
		--menu "Please select:" $HEIGHT $WIDTH 6 \
			"1" "Install essentials" \
			"2" "Install retroarch cores from AUR" \
			"3" "Install retroarch cores from Libretro (testing)" \
			"4" "Install extra packages" \
		2>&1 1>&3)
  	exit_status=$?
  	exec 3>&-
  	case $exit_status in
    		$DIALOG_CANCEL)
			echo $PWD
      			exec ./retroal_setup.sh
      			exit
      		;;
    		$DIALOG_ESC)
      			exec ./retroal_setup.sh
      			exit 1
      		;;
  	esac
  	case $selection in
    	0 )
      		clear
      	;;
    	1 )
		exec ./scripts/core/essentials.sh
      	;;
    	2 )
		exec ./scripts/core/cores.sh
	;;
	3 )
		exec ./scripts/core/cores2.sh
      	;;
	4 )
		exec ./scripts/core/extras.sh
	;;
  	esac
done

