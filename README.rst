RetroAL
=======

=============
What is this?
=============

RetroAL is a set of simple menus to easily install emulators and cores with arch linux (rpi or x86) using AUR branches (WIP: and from official libretro website).

It's similar to retropie but WAY simplier. 

You can use cores available in Arch Linux official repositories and AUR.
Downloading cores from official libretro website will become available shortly.

===================
System Requirements
===================

1) Arch Linux properly installed and configured (maybe in the future, I'll create an image for IoT devices).

2) Your own configurations of network, bluetooth and stuff. RetroAL doesn't have (yet) system configurations like that.

3) Sudo installed and well configured.

=====
Howto
=====

1) Clone this repo in the root of the home folder of the user (root isn't necessary)

2) Enter in the directory and execute retroal_setup.sh

3) Install Retroarch and EmulationStation.  Install Packages > Install Essentials 

4) Install default emulationstation configuration. Install Packages > Install Essentials > Download ES default configurations

5) Install at least one theme for emulationstation (unless you like the mess of not having a theme). Install Packages > Install Essentals > Download ES Themes


====
TODO
====

There's a lot of things that should be done, it's a W.I.P.

===
OBS
===

Since it's using AUR, you can expect unstabilities (specially because I'm trying to do everything quite automatically to use in IoTs or dedicated machines). Use at your own risk.

=============
How to donate
=============

If you find this repo useful (don't forget to pay a visit to the related
repos too), you can buy me a beer:

:BTC: 3ECzX5UhcFSRv6gBBYLNBc7zGP9UA5Ppmn

:ETH: 0x7E17Ac09Fa7e6F80284a75577B5c1cbaAD566C1c

