#!/bin/bash
#
# Setup 0.1
#
# (c) 2018 - Daniel Menelkir <menelkir@itroll.org>

dialog --backtitle "RetroAL - Cores Download" \
        --clear \
        --pause "Keep in mind that some cores aren't available to all platforms." 9 49 3

scripts/start.sh
